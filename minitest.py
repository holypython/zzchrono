from zz_chrono import *
from time import sleep


with  getChrono("sleeping time")  :
     time.sleep(0.1)
     
with  getChrono("sleeping time")  :
     time.sleep(0.2)

display_chrono("sleeping time")

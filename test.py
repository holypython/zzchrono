from zz_chrono import *
import time

@chronometer("printing time")
def print_something():
  print("something")
  
@chronometer("sleeping time")  
def some_sleep():
   time.sleep(0.1)
  
# main program
for i in range(5):
   print_something()
   some_sleep()
   with  getChrono("sleeping time")  :
     time.sleep(0.1)
       
display_all()
